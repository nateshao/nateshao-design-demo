package com.nateshao.design.abstractfactory.impl;

import com.nateshao.design.abstractfactory.ICacheAdapter;
import com.nateshao.design.abstractfactory.matter.EGM;

import java.util.concurrent.TimeUnit;

/**
 * @date Created by 邵桐杰 on 2020/12/9 8:59
 * @微信公众号 千羽的编程时光
 * @个人网站 www.nateshao.cn
 * @博客 https://nateshao.gitee.io
 * @GitHub https://github.com/nateshao
 * @Gitee https://gitee.com/nateshao
 * Description:
 */
public class EGMCacheAdapter implements ICacheAdapter {

    private EGM egm = new EGM();

    public String get(String key) {
        return egm.gain(key);
    }

    public void set(String key, String value) {
        egm.set(key, value);
    }

    public void set(String key, String value, long timeout, TimeUnit timeUnit) {
        egm.setEx(key, value, timeout, timeUnit);
    }

    public void del(String key) {
        egm.delete(key);
    }
}
